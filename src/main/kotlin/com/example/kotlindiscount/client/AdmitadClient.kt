package com.example.kotlindiscount.client

import com.example.discountservicekotlin.model.dto.CouponsDto
import com.example.kotlindiscount.configuration.AdmitadProperties
import com.example.kotlindiscount.model.LoginAdmitad
import com.example.kotlindiscount.service.TokenContainer
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.net.URI

@Service
class AdmitadClient(private val webClient: WebClient,
                    private val ADMITAD_URL:String = "https://api.admitad.com",
                    private val tokenContainer: TokenContainer,
                    private val admitadProperties: AdmitadProperties
) {

    fun getCoupon(websiteId:Long, campaignId:Long): Flux<CouponsDto> {

        val url = URI.create(ADMITAD_URL
                    + "/coupons/website/"
                    + websiteId
                    + "/?campaign="
                    + campaignId
                    + "&limit=500")

        var accessToken = tokenContainer.getToken()?.accessToken

        return webClient.get()
            .uri(url)
            .header(AUTHORIZATION, accessToken)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlux(CouponsDto::class.java);

    }

    fun getToken(): LoginAdmitad? {

        val url = URI.create(ADMITAD_URL + "/token/")

        val login = webClient.post()
            .uri(url)
            .header("grant_type", "client_credentials")
            .header("client_id", admitadProperties.clientId)
            .header("scope", admitadProperties.scope)
            .retrieve()
            .bodyToMono(LoginAdmitad::class.java)
            .block()

        tokenContainer.putToken(login)
        return login;
    }
}