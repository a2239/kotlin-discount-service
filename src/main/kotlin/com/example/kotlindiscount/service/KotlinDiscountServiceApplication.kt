package com.example.kotlindiscount.service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinDiscountServiceApplication

fun main(args: Array<String>) {
	runApplication<KotlinDiscountServiceApplication>(*args)
}
