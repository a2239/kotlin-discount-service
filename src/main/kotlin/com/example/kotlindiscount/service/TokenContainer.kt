package com.example.kotlindiscount.service

import com.example.kotlindiscount.model.LoginAdmitad
import org.springframework.stereotype.Component

@Component
class TokenContainer {
    val tokenCache: HashMap<String, LoginAdmitad?> = hashMapOf();
    companion object{
        const val TOKEN = "token"
    }
    fun getToken(): LoginAdmitad? {
        return tokenCache.get(TOKEN)
    }

    fun putToken(token: LoginAdmitad?){
        tokenCache.put(TOKEN, token)
    }
}