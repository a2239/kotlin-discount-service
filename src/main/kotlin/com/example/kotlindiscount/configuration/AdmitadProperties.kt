package com.example.kotlindiscount.configuration

import org.springframework.beans.factory.annotation.Value

class AdmitadProperties {

    @Value("#{client_id}")
    val clientId: String? = null

    @Value("#{client_secret}")
    val clientSecret: String? = null

    @Value("#{scope}")
    val scope: String? = null

    @Value("#{authorization_url}")
    val authorizationUrl: String? = null

    @Value("#{area_name}")
    val areaName: String? = null

}