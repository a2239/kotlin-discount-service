package com.example.kotlindiscount.repository

import com.example.discountservicekotlin.model.Coupon
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface CouponRepository : ReactiveCrudRepository<Coupon, Long> {
}