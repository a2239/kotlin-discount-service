package com.example.discountservicekotlin.model.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

@JsonIgnoreProperties(ignoreUnknown = true)
class CouponDto {

    private val id: Long? = null

    private val name: String? = null

    private val status: String? = null

    @JsonProperty(value = "campaign")
    private val campaignDto: CampaignDto? = null

    private val description: String? = null

    private val regions: List<String>? = null

    private val discount: String? = null

    private val species: String? = null

    private val promocode: String? = null

    @JsonProperty(value = "frameset_link")
    private val framesetLink: String? = null

    @JsonProperty(value = "goto_link")
    private val gotoLink: String? = null

    @JsonProperty(value = "short_name")
    private val shortName: String? = null

    @JsonProperty(value = "date_start")
    private val dateStart: LocalDateTime? = null

    @JsonProperty(value = "date_end")
    private val dateEnd: LocalDateTime? = null

    @JsonProperty(value = "image")
    private val image: String? = null

}