package com.example.discountservicekotlin.model.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CategoryDto {

    private val id: Long? = null
    private val name: String? = null
    private val language: String? = null
    private val campaignDto: CampaignDto? = null

}