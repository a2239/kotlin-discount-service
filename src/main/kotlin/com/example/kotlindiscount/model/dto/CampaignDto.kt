package com.example.discountservicekotlin.model.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CampaignDto() {

    private val id: Long? = null
    private val name: String? = null
    private val image: String? = null
    private val categories: List<CategoryDto>? = null
    private val description: String? = null
    private val exclusive: Boolean? = null

}
