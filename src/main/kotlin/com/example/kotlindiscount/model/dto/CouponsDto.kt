package com.example.discountservicekotlin.model.dto

import com.fasterxml.jackson.annotation.JsonProperty

class CouponsDto {
    @JsonProperty(value = "results")
    private val coupons: List<CouponDto>? = null
}