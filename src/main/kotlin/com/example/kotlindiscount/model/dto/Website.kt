package com.example.discountservicekotlin.model.dto

import com.fasterxml.jackson.annotation.JsonProperty

class Website {
    @JsonProperty(value = "id")
    private val admitadId: Long? = null

    private val status: String? = null

    private val name: String? = null
}