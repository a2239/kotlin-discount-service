package com.example.kotlindiscount.model

import com.example.discountservicekotlin.model.Category
import com.example.discountservicekotlin.model.Coupon
import com.fasterxml.jackson.annotation.JsonManagedReference
import jakarta.persistence.*
import lombok.Getter
import lombok.Setter
import java.time.LocalDateTime

@Getter
@Setter
@Entity
open class Campaign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null;

    @Column(name = "admitad_id")
    var admitadId: Long? = null;

    @Column
    var name: String? = null;

    @Column
    var imageUrl: String? = null;

    @OneToMany(mappedBy = "partner", cascade = [CascadeType.MERGE, CascadeType.REMOVE])
    @JsonManagedReference
    var coupons: List<Coupon>? = null;

    @Column(nullable = false)
    var lastUpdate: LocalDateTime? = null;

    @Column(nullable = false, updatable = false)
    var createDate: LocalDateTime? = null;

    @OneToMany(mappedBy = "partner", cascade = [CascadeType.ALL])
    @JsonManagedReference
    var categories: MutableSet<Category>? = null;

    @Column
    var description: String? = null;

    @Column
    var exclusive: Boolean? = null;

    @Column
    var image64: String? = null;

    @PreUpdate
    private fun update() {
        lastUpdate = LocalDateTime.now()
    }

    @PrePersist
    private fun create() {
        val now = LocalDateTime.now()
        createDate = now
        lastUpdate = createDate
    }

//    fun addToCategory(category: Category) {
//        category.setCampaign(this)
//        categories!!.add(category)
//    }

}