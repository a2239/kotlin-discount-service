package com.example.discountservicekotlin.model

import com.example.kotlindiscount.model.Campaign
import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.*
import lombok.Getter
import lombok.Setter
import java.time.LocalDateTime


@Entity
@Getter
@Setter
data class Coupon(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    val id: Long,

    @Column(name = "admitad_id")
    var admitadId: Long,

    @Column
    var name: String,

    @Column
    var status: String,

    @JsonProperty(value = "campaign")
    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    @JsonBackReference
    var campaign: Campaign,

    @Column
    var description: String,

//    @BatchSize(size = 100)
//    @ElementCollection(fetch = FetchType.LAZY)
//    @CollectionTable(name = "region", joinColumns = JoinColumn(name = "coupon_id"))
//    @Column(name = "name")
//    var regions: List<String>,

    @Column
    var discount: String,

    @Column
    var species: String,

    @Column
    var promocode: String,

    @Column
    @JsonProperty(value = "frameset_link")
    var framesetLink: String,

    @Column
    @JsonProperty(value = "goto_link")
    var gotoLink: String,

    @Column
    @JsonProperty(value = "short_name")
    var shortName: String,

    @Column
    @JsonProperty(value = "date_start")
    var dateStart: LocalDateTime,

    @Column
    @JsonProperty(value = "date_end")
    var dateEnd: LocalDateTime,

    @Column
    @JsonProperty(value = "image")
    var imageUrl: String,

    @Column(nullable = false)
    var lastUpdate: LocalDateTime,

    @Column(nullable = false, updatable = false)
    var createDate: LocalDateTime,

    @Column
    var image64: String
) {
    
    @PreUpdate
    fun update() {
        lastUpdate = LocalDateTime.now()
    }

    @PrePersist
    fun create() {
        val now = LocalDateTime.now()
        createDate = now
        lastUpdate = createDate
    }
}