package com.example.discountservicekotlin.model

import com.example.kotlindiscount.model.Campaign
import com.fasterxml.jackson.annotation.JsonBackReference
import jakarta.persistence.*

class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    val id: Long? = null

    var name: String? = null

    var admitadId: Long? = null

    var language: String? = null

    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "campaign_id")
    @JsonBackReference
    private val campaign: Campaign? = null
}