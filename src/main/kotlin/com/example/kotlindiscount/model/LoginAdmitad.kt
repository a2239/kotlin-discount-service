package com.example.kotlindiscount.model

import com.fasterxml.jackson.annotation.JsonProperty

class LoginAdmitad {

    @JsonProperty("access_token")
    val accessToken: String? = null
        get() = field;

    @JsonProperty("expires_in")
    val expiresIn: String? = null
        get() = field;

    @JsonProperty("refresh_token")
    val refreshToken: String? = null
        get() = field;

}